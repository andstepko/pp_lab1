#pragma once

//#include "stdafx.h"
#include <cstdlib>

template <class T>
class Matrix
{
public:
	Matrix();
	Matrix(int n);
	Matrix(T** matrix, int n);

	~Matrix(void);
	static void operator delete(void* matrix);

	int getSize();
	T& get(int i, int j);
	T*& get(int i);

	//void set(int i, T t);
	void set(int i, int j, T t);

	void printMatrix();

	Matrix multiply(Matrix b);

private:
	int size;
	T** square;
};

