#include "stdafx.h"
#include "Matrix.h"

template <class T>
Matrix<T>::Matrix(int n)
{
	size = n;
	square = (T**)malloc(n*sizeof(T*));

	for(int i = 0; i < n; i++)
	{
		square[i] = (T*)malloc(n * sizeof(T));
		for(int j = 0; j < n; j++)
		{
			square[i][j] = rand() % 5;
		}
	}
};

template <class T>
Matrix<T>::Matrix()
{};

template <class T>
Matrix<T>::Matrix(T** matrix, int n)
{
	square = matrix;
	size = n;
};

template <class T>
Matrix<T>::~Matrix(void)
{};

template <class T>
void Matrix<T>::operator delete(void* matrix)
{
	Matrix<T>* realMatrix = (Matrix<T>*)matrix;
	for(int i = 0; i < realMatrix->size; i++)
	{
		free(realMatrix->square[i]);
	}
	free(realMatrix->square);
}

template <class T>
int Matrix<T>::getSize()
{
	return size;
};

template <class T>
T& Matrix<T>::get(int i, int j)
{
	return square[i][j];
};

template <class T>
T*& Matrix<T>::get(int i)
{
	return square[i];
};

template <class T>
void Matrix<T>::set(int i, int j, T t)
{
	square[i][j] = t;
};

template <class T>
void Matrix<T>::printMatrix()
{
	for(int i = 0; i < size; i++)
	{
		for(int j = 0; j < size; j++)
		{
			printf("%0.2f ", square[i][j]);
		}
		printf("\n");
	}
}

template <class T>
Matrix<T> Matrix<T>::multiply(Matrix<T> b)
{
	if(getSize() != b.getSize())
	{
		return 0;
	}

	int n = getSize();

	Matrix<T>* result = new Matrix<T>();
	result[0].square = (T**)malloc(n*sizeof(T*));

	for(int i = 0; i < n; i++)
	{
		result[0].get(i) = (T*)calloc(n, sizeof(T));
		for(int j = 0; j < n; j++)
		{
			T t = get(i, j);
			for(int k = 0; k < n; k++)
			{
				result[0].set(i, k, result[0].get(i, j) + t * b.get(j, k));
			}
		}
	}
	return result[0];
};