// Lab1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include <time.h>

#include <iostream>
#include "Matrix.h"
#include "Matrix.cpp"

using namespace std;

void printTaskSeparator(int i)
{
	printf("\n================================_%d_================================\n", i);
}

//1
void printYearMonDay(__time64_t time)
{
	struct tm tmStruct;
	_gmtime64_s(&tmStruct, &time);
	printf("Last 32ibit time: year==>%d month==>%d day==>%d\n", tmStruct.tm_year + 1900,
		tmStruct.tm_mon + 1, tmStruct.tm_mday);
}

//2
void printSystemTimeAsFileTimeAccurancy()
{
	FILETIME fileTime;
	FILETIME fileTime2;
	DWORD count = 0;

	GetSystemTimeAsFileTime(&fileTime);
	GetSystemTimeAsFileTime(&fileTime2);

	while(fileTime2.dwLowDateTime - fileTime.dwLowDateTime == 0){
		GetSystemTimeAsFileTime(&fileTime2);
		count++;
	}
	printf("GetSystemTimeAsFileTime: tick==>%f ms, loop count==>%u\n",
		(fileTime2.dwLowDateTime - fileTime.dwLowDateTime) / 10000.0, count);
}

void printGetTickAccurancy()
{
	DWORD time1;
	DWORD time2;
	DWORD count = 0;
	
	time1 = GetTickCount();
	time2 = GetTickCount();

	while(time2 - time1 == 0){
		time2 = GetTickCount();
		count++;
	}
	printf("GetTickAccurancy: tick==>%u ms, loop count==>%u", time2 - time1, count);
}

//3
DWORD64 sum(DWORD* data, int n)
{
	DWORD64 sum = 0;
	for(int i = 0; i < n; i++)
	{
		sum += data[i];
	}
	return sum;
}

inline unsigned __int64 getAssemblerTacts()
{
	__asm
	{
		mov eax, 0
		cpuid
		rdtsc
	}
}

unsigned __int64 printAssemblerTime(DWORD* data, int n)
{
	__int64 start;
	__int64 finish;

	start = getAssemblerTacts();
	DWORD64 summa = sum(data, n);
	finish = getAssemblerTacts();

	DWORD64 diff = finish - start;

	printf("Assembler takts consumed==>%I64u, elements==>%i, sum==>%I64u\n", diff, n, sum);

	return diff;
}

DWORD64 printRdcstTime(DWORD* data, int n)
{
	DWORD64 start;
	DWORD64 finish;

	start = __rdtsc();
	DWORD64 summa = sum(data, n);
	finish = __rdtsc();

	DWORD64 diff = finish - start;

	printf("rdtsc takts consumed==>%I64u, elements==>%i, sum==>%I64u\n", diff, n, sum);

	return diff;
}

__int64 printQueryPerformTime(DWORD* data, int n)
{
	__int64 start = 0, finish = 0, frequency = 0;

	QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);

	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	DWORD summa = sum(data, n);
	QueryPerformanceCounter((LARGE_INTEGER *)&finish);

	__int64 diff = (finish - start);
	
	printf("QueryPerform time consumed==>%f ms, sum==>%I64u\n", diff * 1000.0 / frequency, sum);

	return diff;
}

//5
template<class T>
T** notAstupidMultiply(T** a, T** b, int n)
{
	T** result = (T**)malloc(n*sizeof(T*));

	for(int i = 0; i < n; i++)
	{
		result[i] = (T*)calloc(n, sizeof(T));
		for(int j = 0; j < n; j++)
		{
			T t = a[i][j];
			for(int k = 0; k < n; k++)
			{
				result[i][k] += t * b[j][k];
			}
		}
	}
	return result;
}

template <class T>
void ObjNoObjComparation()
{
	__int64 start2 = 0;
	__int64 finish2 = 0;
	__int64 frequency = 0;
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);

	for(int i = 512; i < 4096; i*=2)
	{
		//for(int attempt = 0; attempt < 10; attempt++)

		//Objects
		Matrix<T>* a = new Matrix<T>(i);
		Matrix<T>* b = new Matrix<T>(i);

		QueryPerformanceCounter((LARGE_INTEGER *)&start2);
		a->multiply(*b);
		QueryPerformanceCounter((LARGE_INTEGER *)&finish2);
		__int64 diffObj = (finish2 - start2);
		printf("%i. Objects. Time consumed==>%f ms\n", i, diffObj * 1000.0 / frequency);
		delete(a);
		delete(b);

		//No objects
		T** matrix1 = generateRandomMatrix<T>(i);
		T** matrix2 = generateRandomMatrix<T>(i);
		T** stupMult;

		QueryPerformanceCounter((LARGE_INTEGER *)&finish2);
		stupMult = notAstupidMultiply(matrix1, matrix2, i);
		QueryPerformanceCounter((LARGE_INTEGER *)&finish2);
		__int64 diffNoObj = (finish2 - start2);
		printf("%i. No objects. Time consumed==>%f ms\n", i, diffNoObj * 1000.0 / frequency);

		for(int curr = 0; curr < i; curr++)
		{
			free(matrix1[curr]);
			free(matrix2[curr]);
		}
		free(matrix1);
		free(matrix2);

		printf("Objects are %f times worse!\n", ((double)diffNoObj) / diffObj);
		printf("-----------------------\n");
	}
}

template <class T>
T** generateRandomMatrix(int n)
{
	T** matrix = (T**)malloc(n*sizeof(T*));

	for(int i = 0; i < n; i++)
	{
		matrix[i] = (T*)malloc(n * sizeof(T));
		for(int j = 0; j < n; j++)
		{
			matrix[i][j] = rand() % 5;
		}
	}

	return matrix;
}

int _tmain(int argc, _TCHAR* argv[])
{
	//// task1
	//printTaskSeparator(1);
	//__time64_t time32 = 0x7FFFFFFF;
	//printYearMonDay(time32);
	//
	////task2
	//printTaskSeparator(2);
	//printSystemTimeAsFileTimeAccurancy();
	//printGetTickAccurancy();

	////task3
	//printTaskSeparator(3);
	//// Fill array.
	//int n = 100000;
	//DWORD* data = new DWORD[n*3];
	//for(DWORD i = 0; i < 3*n; i++)
	//{
	//	data[i] = rand() % 100;
	//}

	//for(int i = 1; i < 4; i++)
	//{
	//	printf("%i\n", i*n);
	//	printAssemblerTime(data, i*n);
	//	printRdcstTime(data, i*n);
	//	printQueryPerformTime(data, i*n);
	//}

	////task4
	//printTaskSeparator(4);
	//printf("Absolute\n");
	//DWORD64 rdtsc1 = printRdcstTime(data, n);
	//DWORD64 rdtsc2 = printRdcstTime(data, n*2);
	//DWORD64 rdtsc3 = printRdcstTime(data, n*3);
	//printf("2/1==>%f, 3/1==>%f", ((double) rdtsc2) / rdtsc1, ((double) rdtsc3) / rdtsc1);
	//printf("Relevant\n");
	//DWORD start;
	//DWORD finish;
	//DWORD max = 2000;
	//DWORD count;
	//DWORD* cycles = new DWORD[3];

	//for(int i = 1; i < 4; i++)
	//{
	//	count = 0;
	//	printf("%i: ", i*n);
	//	finish = GetTickCount();
	//	start = GetTickCount();
	//	while(finish - start < max)
	//	{
	//		sum(data, i*n);
	//		finish = GetTickCount();
	//		count++;
	//	}
	//	cycles[i] = count;
	//	printf("cycles==>%i, time consumed==>%i ms\n", count, finish - start);
	//}
	//printf("2/1==>%f, 3/1==>%f\n", ((double) cycles[1]) / cycles[2], ((double) cycles[1]) / cycles[3]);




	//task5
	printTaskSeparator(5);

	printf("__int8\n");
	ObjNoObjComparation<__int8>();
	printf("__int16\n");
	ObjNoObjComparation<__int16>();
	printf("__int32\n");
	ObjNoObjComparation<__int32>();
	printf("__int64\n");
	ObjNoObjComparation<__int64>();
	printf("float\n");
	ObjNoObjComparation<float>();
	printf("double\n");
	ObjNoObjComparation<double>();


	getchar();
	return 0;
}

